//
//  IntroductionViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/13.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit
import CoreData

// This is the introduction page view which is responsible for telling the user what this application can do and how to use this software. Users can select whether show this page or not.
class IntroductionViewController: UIViewController {
    
    // Used to read from the core data
    var managedObjectContext:NSManagedObjectContext?
    // Used to store the user's selection
    var setting:[Setting]?
    
    @IBOutlet weak var skipButtonField: UIButton!
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var neverButtonField: UIButton!
    
    // This function is invoked when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setting of the 'skip' button
        skipButtonField.backgroundColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        let attributedString = NSMutableAttributedString(string:"")
        let attrs = [NSFontAttributeName :UIFont.boldSystemFont(ofSize:17.0),
                     NSForegroundColorAttributeName :UIColor(red: 75/255, green:75/225, blue: 73/255, alpha: 1), NSUnderlineStyleAttributeName :0]as [String :Any]
        let Setstr = NSMutableAttributedString.init(string: "Skip", attributes: attrs)
        attributedString.append(Setstr)
        self.skipButtonField.setAttributedTitle(attributedString, for: .normal)
        
        // Setting of the 'never show later' button
        neverButtonField.backgroundColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        let attributedString2 = NSMutableAttributedString(string:"")
        let attrs2 = [NSFontAttributeName :UIFont.boldSystemFont(ofSize:17.0),
                     NSForegroundColorAttributeName :UIColor(red: 75/255, green:75/225, blue: 73/255, alpha: 1), NSUnderlineStyleAttributeName :0]as [String :Any]
        let Setstr2 = NSMutableAttributedString.init(string: "Don't Show Me Again", attributes: attrs2)
        attributedString2.append(Setstr2)
        self.neverButtonField.setAttributedTitle(attributedString2, for: .normal)
        
        // Initial the core data and get the storage
        let appDele = UIApplication.shared.delegate as! AppDelegate
        self.managedObjectContext = appDele.persistentContainer.viewContext
        fetchData()
        
        // Choose how this page will be shown according to the user's selection
        if self.setting?.isEmpty == true
        {
            self.backgroundView.isHidden = true
            self.imageView.isHidden = true
        }
        else
        {
            self.backgroundView.isHidden = false
            self.imageView.isHidden = false
            
            print(self.setting![0].show)
            // Set the displaying time of the splash screen to 0.1 seconds
            perform(#selector(IntroductionViewController.showNavController), with: nil, afterDelay: 0.1)
        }
    }
    
    // It will be called after the 2-second-display and navigate to another view
    func showNavController()
    {
        performSegue(withIdentifier: "showIntroductionScreenSegue", sender: self)
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // This function will be called in the viewDidLoad function and the viewWillAppear function,
    // mainly used to catch the core data and store the core data into the categories.
    func fetchData(){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Setting")
        
        do {
            let fetchResults = try managedObjectContext?.fetch(fetch) as! [Setting]
            setting = fetchResults
            print(self.setting!)
        } catch {
            fatalError("Failed to fetch appointments: \(error)")
        }
    }
    
    // If the user choose not to show this page later, user's setting will be stored into the core data
    @IBAction func never(_ sender: UIButton) {
        let singleSetting:Setting
        singleSetting = (NSEntityDescription.insertNewObject(forEntityName: "Setting", into: managedObjectContext!) as? Setting)!
        singleSetting.show = false
        //save the ManagedOnjectContext
        do
        {
            try self.managedObjectContext?.save()
        }
        catch let error
        {
            print("Could not insert \(error)")
        }
    }
}
