//
//  AnimalTypeSelectionViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/4.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This view is used for users to select which animal type they want to display. All data will be returned to the originating view
class AnimalTypeSelectionViewController: UIViewController {

    @IBOutlet weak var allSwitch: UISwitch!
    @IBOutlet weak var antSwitch: UISwitch!
    @IBOutlet weak var beeSwitch: UISwitch!
    @IBOutlet weak var marsupialSwitch: UISwitch!
    @IBOutlet weak var snakeSwitch: UISwitch!
    @IBOutlet weak var spiderSwitch: UISwitch!
    @IBOutlet weak var waspSwitch: UISwitch!
    
    // Used to store the selected animal type to display
    var selectedAnimal:[String] = []
    
    // Once the view is loaded, this function will be called
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the color of the navigation bar
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor
        
        // Set the state of the switch button when the view is loaded
        if selectedAnimal.count == 0
        {
            antSwitch.setOn(false, animated: true)
            beeSwitch.setOn(false, animated: true)
            marsupialSwitch.setOn(false, animated: true)
            snakeSwitch.setOn(false, animated: true)
            spiderSwitch.setOn(false, animated: true)
            waspSwitch.setOn(false, animated: true)
            allSwitch.setOn(false, animated: true)
        }
        else
        {
            var count:Int = 0
            if selectedAnimal.contains("Ants")
            {
                antSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                antSwitch.setOn(false, animated: true)
            }
            if selectedAnimal.contains("Bees")
            {
                beeSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                beeSwitch.setOn(false, animated: true)
            }
            if selectedAnimal.contains("Carnivorous Marsupials")
            {
                marsupialSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                marsupialSwitch.setOn(false, animated: true)
            }
            if selectedAnimal.contains("Snakes")
            {
                snakeSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                snakeSwitch.setOn(false, animated: true)
            }
            if selectedAnimal.contains("Spiders")
            {
                spiderSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                spiderSwitch.setOn(false, animated: true)
            }
            if selectedAnimal.contains("Wasps")
            {
                waspSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                waspSwitch.setOn(false, animated: true)
            }
            if count == 6
            {
                allSwitch.setOn(true, animated: true)
            }
            else
            {
                allSwitch.setOn(false, animated: true)
            }
        }
        
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Set the function to be invoked when the cancel button is clicked
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Do some validations before performing the segue
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "unwindToSearchAnimalSegue"
        {
            if antSwitch.isOn == false, beeSwitch.isOn == false, marsupialSwitch.isOn == false, snakeSwitch.isOn == false, spiderSwitch.isOn == false, waspSwitch.isOn == false
            {
                self.showAlertWithDismiss(title: "Selection Required", message: "You must select at least one animal to display.")
                return false
            }
        }
        return true
    }
    
    // Set the alert function, so that it can be called when this function is called
    func showAlertWithDismiss(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertDismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertDismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Set the function to be invoked when the save button is clicked
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToSearchAnimalSegue"
        {
            selectedAnimal = []
            if allSwitch.isOn == true
            {
                selectedAnimal.append("Ants")
                selectedAnimal.append("Bees")
                selectedAnimal.append("Carnivorous Marsupials")
                selectedAnimal.append("Snakes")
                selectedAnimal.append("Spiders")
                selectedAnimal.append("Wasps")
            }
            else
            {
                if antSwitch.isOn == true
                {
                    selectedAnimal.append("Ants")
                }
                if beeSwitch.isOn == true
                {
                    selectedAnimal.append("Bees")
                }
                if marsupialSwitch.isOn == true
                {
                    selectedAnimal.append("Carnivorous Marsupials")
                }
                if snakeSwitch.isOn == true
                {
                    selectedAnimal.append("Snakes")
                }
                if spiderSwitch.isOn == true
                {
                    selectedAnimal.append("Spiders")
                }
                if waspSwitch.isOn == true
                {
                    selectedAnimal.append("Wasps")
                }
            }
        }
    }
    
    // Define what other switch button should do when the all switch is clicked
    @IBAction func allSwitchChanged(_ sender: UISwitch) {
        if allSwitch.isOn == true
        {
            antSwitch.setOn(true, animated: true)
            beeSwitch.setOn(true, animated: true)
            marsupialSwitch.setOn(true, animated: true)
            snakeSwitch.setOn(true, animated: true)
            spiderSwitch.setOn(true, animated: true)
            waspSwitch.setOn(true, animated: true)
        }
        if allSwitch.isOn == false
        {
            antSwitch.setOn(false, animated: true)
            beeSwitch.setOn(false, animated: true)
            marsupialSwitch.setOn(false, animated: true)
            snakeSwitch.setOn(false, animated: true)
            spiderSwitch.setOn(false, animated: true)
            waspSwitch.setOn(false, animated: true)
        }
    }
    
    // Define what other switch button should do when the ant switch is clicked
    @IBAction func antSwitchChanged(_ sender: UISwitch) {
        if antSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if antSwitch.isOn == true, beeSwitch.isOn == true, marsupialSwitch.isOn == true, snakeSwitch.isOn == true, spiderSwitch.isOn == true, waspSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }

    // Define what other switch button should do when the bee switch is clicked
    @IBAction func beeSwitchChanged(_ sender: UISwitch) {
        if beeSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if antSwitch.isOn == true, beeSwitch.isOn == true, marsupialSwitch.isOn == true, snakeSwitch.isOn == true, spiderSwitch.isOn == true, waspSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }

    // Define what other switch button should do when the marsupial switch is clicked
    @IBAction func marsupialSwitchChanged(_ sender: UISwitch) {
        if marsupialSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if antSwitch.isOn == true, beeSwitch.isOn == true, marsupialSwitch.isOn == true, snakeSwitch.isOn == true, spiderSwitch.isOn == true, waspSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
    
    // Define what other switch button should do when the snake switch is clicked
    @IBAction func snakeSwitchChanged(_ sender: UISwitch) {
        if snakeSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if antSwitch.isOn == true, beeSwitch.isOn == true, marsupialSwitch.isOn == true, snakeSwitch.isOn == true, spiderSwitch.isOn == true, waspSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
    
    // Define what other switch button should do when the spider switch is clicked
    @IBAction func spiderSwitchChanged(_ sender: UISwitch) {
        if spiderSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if antSwitch.isOn == true, beeSwitch.isOn == true, marsupialSwitch.isOn == true, snakeSwitch.isOn == true, spiderSwitch.isOn == true, waspSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
    
    // Define what other switch button should do when the wasp switch is clicked
    @IBAction func waspSwitchChanged(_ sender: UISwitch) {
        if waspSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if antSwitch.isOn == true, beeSwitch.isOn == true, marsupialSwitch.isOn == true, snakeSwitch.isOn == true, spiderSwitch.isOn == true, waspSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
}
