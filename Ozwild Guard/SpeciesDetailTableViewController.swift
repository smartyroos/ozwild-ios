//
//  SpeciesDetailTableViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/1.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This view is used to display all detailed information about a certain species
class SpeciesDetailTableViewController: UITableViewController {

    // Used to stored the common name of the species
    var speciesCommonName:String = ""
    // Used to stored the scientific name of the species
    var speciesScientificName:String = ""
    // Used to stored the dangerous description of the species
    var speciesDangerousDes:String = ""
    // Used to stored the description of the species
    var speciesDes:String = ""
    
    // Once the view is loaded, this function will be called
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the color of the navigation bar
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor
        // Disable the user interation with the screen
        self.view.isUserInteractionEnabled = false
        self.navigationItem.title = self.speciesCommonName
        
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // set the number of sections for this table view
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    // Set the number of rows for each secrtion
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    // Set the height of each header for each section
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 40
        }
        else
        {
            return 15
        }
    }
    
    // Set the height of each footer for each section
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    // Set the title for each header in different sections
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0
        {
            return "Scientific Name: \(self.speciesScientificName)"
        }
        else if section == 1
        {
            return "Venomous Level --"
        }
        else
        {
            return "Species Description --"
        }
    }
    
    // Set the height of each row in the table
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return 210
        }
        else if indexPath.section == 1
        {
            return 120
        }
        else
        {
            return 155
        }
    }
    
    // Set the content for each row in every section
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScientificNameCell", for: indexPath) as! DetailSpeciesScientificTableViewCell
            cell.imageField.image = UIImage(named: self.speciesScientificName)
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VenomousLevelCell", for: indexPath) as! VenomousLevelTableViewCell
            cell.textField.text = self.speciesDangerousDes
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as! DescriptionTableViewCell
            cell.textField.text = self.speciesDes
            return cell
        }
    }
    
    // Set the layout of all headers in the table
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        header.textLabel?.textColor = UIColor.darkGray
    }
    
    // Define the cancel button
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}
