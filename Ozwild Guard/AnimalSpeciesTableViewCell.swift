//
//  AnimalSpeciesTableViewCell.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/3.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

class AnimalSpeciesTableViewCell: UITableViewCell {

    @IBOutlet weak var animalDangerousTextField: UILabel!
    @IBOutlet weak var animalDangerousList: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
