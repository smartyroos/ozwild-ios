//
//  Setting+CoreDataProperties.swift
//  
//
//  Created by 吴慧超 on 2017/5/14.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Setting {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Setting> {
        return NSFetchRequest<Setting>(entityName: "Setting");
    }

    @NSManaged public var show: Bool

}
