//
//  SearchAnimalViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/3.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

// Icon created by Arthur Shlain from Noun Project
// Icon created by David Swanson from Noun Project

import UIKit
import Firebase

//  This is the third view for the tab. This view is responsible for dealing with three main functions: searching for one or more animals stored in the database accoding to user-defined animal type, searching for one or more animals stored in the database accoding to user-defined danger level, and searching for one or more animals stored in the database accoding to user-defined sub-string of the animal common name.
class SearchAnimalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate{

    @IBOutlet weak var selectionField: UITableView!
    @IBOutlet weak var listField: UITableView!
    @IBOutlet weak var activityWait: UIActivityIndicatorView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    // Claim the firebase so that i can use it
    var databaseRef = FIRDatabase.database().reference()
    var viewThisOccurenceData: NSArray?
    
    // Used to store the user-defined selection about the danger level and animal type for the search
    var antSelection:[Animal] = []
    var beeSelection:[Animal] = []
    var marsupialSelection:[Animal] = []
    var snakeSelection:[Animal] = []
    var spiderSelection:[Animal] = []
    var waspSelection:[Animal] = []
    var speciesCount:Int = 0
    // By default, all species should be selected
    var selectedAnimal:[String] = ["Ants", "Bees", "Carnivorous Marsupials", "Snakes", "Spiders", "Wasps"]
    var selectedDangerousLevel:[String] = ["3", "2", "1"]
    
    // Used to save all retrieved data so that users only need to read from the database once and the local data can be used later on
    var antRetrieved:[Animal] = []
    var beeRetrieved:[Animal] = []
    var marsupialRetrieved:[Animal] = []
    var snakeRetrieved:[Animal] = []
    var spiderRetrieved:[Animal] = []
    var waspRetrieved:[Animal] = []
    var categoryScienceName:[String] = []
    
    // Used to check when the user click the section title, to see whether the section is collapsed or expanded
    var selectedSections:[Int]!
    // Used to check whether this view is loaded for the first time (if it's the first time, no sections will be shown
    var checkState = false
    
    // This function is invoked when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor
        self.selectionField.delegate = self
        self.selectionField.dataSource = self
        
        self.listField.delegate = self
        self.listField.dataSource = self
        
        // Set the header layout for the table
        self.listField.sectionHeaderHeight = 0
        self.listField.sectionFooterHeight = 0
        
        // Set the header layout for the table
        self.selectionField.sectionHeaderHeight = 0
        self.selectionField.sectionFooterHeight = 0
        
        self.searchField.delegate = self
        // Always show the clear button
        self.searchField.clearButtonMode = .always
        // Sign in anonymously the database
        FIRAuth.auth()?.signInAnonymously() { (user, error) in
        }
        
        self.selectedSections = []
        
        searchButton.backgroundColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        let attributedString = NSMutableAttributedString(string:"")
        let attrs = [NSFontAttributeName :UIFont.systemFont(ofSize:15.0),
                     NSForegroundColorAttributeName :UIColor.black, NSUnderlineStyleAttributeName :0]as [String :Any]
        let Setstr = NSMutableAttributedString.init(string: "Search", attributes: attrs)
        attributedString.append(Setstr)
        self.searchButton.setAttributedTitle(attributedString, for: .normal)
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Used to dismiss the keyboard when other space is clicked
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(false)
    }
    
    // When the 'return' button is clicked the kayboard will be dismissed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Set the height of the header for each section
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == selectionField
        {
            if section == 0
            {
                return 30
            }
            else
            {
                return 0
            }
        }
        else
        {
            if section == 0
            {
                if antSelection.isEmpty == true
                {
                    return 0.1
                }
                else
                {
                    return 38
                }
            }
            if section == 1
            {
                if beeSelection.isEmpty == true
                {
                    return 0.1
                }
                else
                {
                    return 38
                }
            }
            if section == 2
            {
                if marsupialSelection.isEmpty == true
                {
                    return 0.1
                }
                else
                {
                    return 38
                }
            }
            if section == 3
            {
                if snakeSelection.isEmpty == true
                {
                    return 0.1
                }
                else
                {
                    return 38
                }
            }
            if section == 4
            {
                if spiderSelection.isEmpty == true
                {
                    return 0.1
                }
                else
                {
                    return 38
                }
            }
            if section == 5
            {
                if waspSelection.isEmpty == true
                {
                    return 0.1
                }
                else
                {
                    return 38
                }
            }
            else
            {
                return 0
            }
        }
    }
    
    // Set the height of the header for each section
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == selectionField
        {
            return 0
        }
        else
        {
            return 0
        }
    }
    
    // set the number of sections for this table view
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.selectionField
        {
            return 2
        }
        else
        {
            return 6
        }
    }
    
    // Set the number of rows for each secrtion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.selectionField
        {
            return 1
        }
        else
        {
            if self.selectedSections.contains(section)
            {
                return 0
            }
            else
            {
                if section == 0
                {
                    return antSelection.count
                }
                if section == 1
                {
                    return beeSelection.count
                }
                if section == 2
                {
                    return marsupialSelection.count
                }
                if section == 3
                {
                    return snakeSelection.count
                }
                if section == 4
                {
                    return spiderSelection.count
                }
                else
                {
                    return waspSelection.count
                }
            }
        }
    }
    
    // Set the title for each header in different sections
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.listField
        {
            if section == 0
            {
                if antSelection.count == 0
                {
                    return ""
                }
                return "Ants                                                                              >"
                
            }
            if section == 1
            {
                if beeSelection.count == 0
                {
                    return ""
                }
                return "Bees                                                                               >"
            }
            if section == 2
            {
                if marsupialSelection.count == 0
                {
                    return ""
                }
                return "Carnivorous Marsupials                                  >"
            }
            if section == 3
            {
                if snakeSelection.count == 0
                {
                    return ""
                }
                return "Snakes                                                                         >"
            }
            if section == 4
            {
                if spiderSelection.count == 0
                {
                    return ""
                }
                return "Spiders                                                                        >"
            }
            else
            {
                if waspSelection.count == 0
                {
                    return ""
                }
                return "Wasps                                                                          >"
            }
        }
        else
        {
            return "Filter:"
        }
    }
    
    // Add a tap funtion into the header for each section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UITableViewHeaderFooterView()
        if tableView == self.listField
        {
            // If the view is loaded for the first time, the tap funtion won't be added.
            if checkState == true
            {
                headerView.tag = section
                
                // The tap function will be invoked with only one click
                let tagrecog = UITapGestureRecognizer(target: self, action: #selector(taped))
                tagrecog.numberOfTapsRequired = 1
                tagrecog.numberOfTouchesRequired = 1
                
                headerView.addGestureRecognizer(tagrecog)
            }
        }
        return headerView
    }
    
    // The tap function which is added to the header of each section (used for collapse or expand the section)
    func taped(gestureRecogniser:UITapGestureRecognizer)
    {
        let view = gestureRecogniser.view
        // Check whether the current section is expanded or not
        if selectedSections.contains((view?.tag)!)
        {
            var index = 0
            var position:Int!
            while index < selectedSections.count
            {
                if selectedSections[index] == view?.tag
                {
                    position = index
                }
                index = index + 1
            }
            selectedSections.remove(at: position)
        }
        else
        {
            selectedSections.append((view?.tag)!)
        }
        self.listField.reloadData()
    }
    
    // Set the cell details for each row in each section separately
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.listField
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AntCell", for: indexPath) as! ListSpeciesSearchTableViewCell
                cell.scientificNameCell.text = self.antSelection[indexPath.row].scienceName
                cell.commonNameCell.text = self.antSelection[indexPath.row].commonName
                cell.imageDisplayCell.image = UIImage.init(named: self.antSelection[indexPath.row].scienceName)
                if self.antSelection[indexPath.row].dangerousLevel == "3"
                {
                    cell.dangerousLevelCell?.text = "High"
                    cell.dangerousLevelCell?.textColor = UIColor.red
                }
                if self.antSelection[indexPath.row].dangerousLevel == "2"
                {
                    cell.dangerousLevelCell?.text = "Medium"
                    cell.dangerousLevelCell?.textColor = UIColor.orange
                }
                if self.antSelection[indexPath.row].dangerousLevel == "1"
                {
                    cell.dangerousLevelCell?.text = "Low"
                    cell.dangerousLevelCell?.textColor = UIColor(red: 6.0/255.0, green: 165.0/255.0, blue: 53.0/255.0, alpha: 1.0)
                }
                return cell
            }
            if indexPath.section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BeeCell", for: indexPath) as! ListSpeciesSearchTableViewCell
                cell.scientificNameCell.text = self.beeSelection[indexPath.row].scienceName
                cell.commonNameCell.text = self.beeSelection[indexPath.row].commonName
                cell.imageDisplayCell.image = UIImage.init(named: self.beeSelection[indexPath.row].scienceName)
                if self.beeSelection[indexPath.row].dangerousLevel == "3"
                {
                    cell.dangerousLevelCell?.text = "High"
                    cell.dangerousLevelCell?.textColor = UIColor.red
                }
                if self.beeSelection[indexPath.row].dangerousLevel == "2"
                {
                    cell.dangerousLevelCell?.text = "Medium"
                    cell.dangerousLevelCell?.textColor = UIColor.orange
                }
                if self.beeSelection[indexPath.row].dangerousLevel == "1"
                {
                    cell.dangerousLevelCell?.text = "Low"
                    cell.dangerousLevelCell?.textColor = UIColor(red: 6.0/255.0, green: 165.0/255.0, blue: 53.0/255.0, alpha: 1.0)
                }
                return cell
            }
            if indexPath.section == 2
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MarsupialCell", for: indexPath) as! ListSpeciesSearchTableViewCell
                cell.scientificNameCell.text = self.marsupialSelection[indexPath.row].scienceName
                cell.commonNameCell.text = self.marsupialSelection[indexPath.row].commonName
                cell.imageDisplayCell.image = UIImage.init(named: self.marsupialSelection[indexPath.row].scienceName)
                if self.marsupialSelection[indexPath.row].dangerousLevel == "3"
                {
                    cell.dangerousLevelCell?.text = "High"
                    cell.dangerousLevelCell?.textColor = UIColor.red
                }
                if self.marsupialSelection[indexPath.row].dangerousLevel == "2"
                {
                    cell.dangerousLevelCell?.text = "Medium"
                    cell.dangerousLevelCell?.textColor = UIColor.orange
                }
                if self.marsupialSelection[indexPath.row].dangerousLevel == "1"
                {
                    cell.dangerousLevelCell?.text = "Low"
                    cell.dangerousLevelCell?.textColor = UIColor(red: 6.0/255.0, green: 165.0/255.0, blue: 53.0/255.0, alpha: 1.0)
                }
                return cell
            }
            if indexPath.section == 3
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SnakeCell", for: indexPath) as! ListSpeciesSearchTableViewCell
                cell.scientificNameCell.text = self.snakeSelection[indexPath.row].scienceName
                cell.commonNameCell.text = self.snakeSelection[indexPath.row].commonName
                cell.imageDisplayCell.image = UIImage.init(named: self.snakeSelection[indexPath.row].scienceName)
                if self.snakeSelection[indexPath.row].dangerousLevel == "3"
                {
                    cell.dangerousLevelCell?.text = "High"
                    cell.dangerousLevelCell?.textColor = UIColor.red
                }
                if self.snakeSelection[indexPath.row].dangerousLevel == "2"
                {
                    cell.dangerousLevelCell?.text = "Medium"
                    cell.dangerousLevelCell?.textColor = UIColor.orange
                }
                if self.snakeSelection[indexPath.row].dangerousLevel == "1"
                {
                    cell.dangerousLevelCell?.text = "Low"
                    cell.dangerousLevelCell?.textColor = UIColor(red: 6.0/255.0, green: 165.0/255.0, blue: 53.0/255.0, alpha: 1.0)
                }
                return cell
            }
            if indexPath.section == 4
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SpiderCell", for: indexPath) as! ListSpeciesSearchTableViewCell
                cell.scientificNameCell.text = self.spiderSelection[indexPath.row].scienceName
                cell.commonNameCell.text = self.spiderSelection[indexPath.row].commonName
                cell.imageDisplayCell.image = UIImage.init(named: self.spiderSelection[indexPath.row].scienceName)
                if self.spiderSelection[indexPath.row].dangerousLevel == "3"
                {
                    cell.dangerousLevelCell?.text = "High"
                    cell.dangerousLevelCell?.textColor = UIColor.red
                }
                if self.spiderSelection[indexPath.row].dangerousLevel == "2"
                {
                    cell.dangerousLevelCell?.text = "Medium"
                    cell.dangerousLevelCell?.textColor = UIColor.orange
                }
                if self.spiderSelection[indexPath.row].dangerousLevel == "1"
                {
                    cell.dangerousLevelCell?.text = "Low"
                    cell.dangerousLevelCell?.textColor = UIColor(red: 6.0/255.0, green: 165.0/255.0, blue: 53.0/255.0, alpha: 1.0)
                }
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "WaspCell", for: indexPath) as! ListSpeciesSearchTableViewCell
                cell.scientificNameCell.text = self.waspSelection[indexPath.row].scienceName
                cell.commonNameCell.text = self.waspSelection[indexPath.row].commonName
                cell.imageDisplayCell.image = UIImage.init(named: self.waspSelection[indexPath.row].scienceName)
                if self.waspSelection[indexPath.row].dangerousLevel == "3"
                {
                    cell.dangerousLevelCell?.text = "High"
                    cell.dangerousLevelCell?.textColor = UIColor.red
                }
                if self.waspSelection[indexPath.row].dangerousLevel == "2"
                {
                    cell.dangerousLevelCell?.text = "Medium"
                    cell.dangerousLevelCell?.textColor = UIColor.orange
                }
                if self.waspSelection[indexPath.row].dangerousLevel == "1"
                {
                    cell.dangerousLevelCell?.text = "Low"
                    cell.dangerousLevelCell?.textColor = UIColor(red: 6.0/255.0, green: 165.0/255.0, blue: 53.0/255.0, alpha: 1.0)
                }
                return cell
            }
        }
        else
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AnimalTypeCell", for: indexPath) as! AnimalTypeTableViewCell
                var animalTypeList:String = ""
                if selectedAnimal.contains("Ants")
                {
                    animalTypeList = animalTypeList + "Ant "
                }
                if selectedAnimal.contains("Bees")
                {
                    animalTypeList = animalTypeList + "Bee "
                }
                if selectedAnimal.contains("Carnivorous Marsupials")
                {
                    animalTypeList = animalTypeList + "Marsupial "
                }
                if selectedAnimal.contains("Snakes")
                {
                    animalTypeList = animalTypeList + "Snake "
                }
                if selectedAnimal.contains("Spiders")
                {
                    animalTypeList = animalTypeList + "Spider "
                }
                if selectedAnimal.contains("Wasps")
                {
                    animalTypeList = animalTypeList + "Wasp "
                }
                cell.animalTpyeList.text = animalTypeList
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AnimalDangerousCell", for: indexPath) as! AnimalSpeciesTableViewCell
                var animalDangerousList:String = ""
                if selectedDangerousLevel.contains("3")
                {
                    animalDangerousList = animalDangerousList + "High "
                }
                if selectedDangerousLevel.contains("2")
                {
                    animalDangerousList = animalDangerousList + "Medium "
                }
                if selectedDangerousLevel.contains("1")
                {
                    animalDangerousList = animalDangerousList + "Low "
                }
                cell.animalDangerousList.text = animalDangerousList
                return cell
            }
        }
    }
    
    // Set the height of each row in the table
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == listField
        {
            return 75
        }
        return UITableViewAutomaticDimension
    }
    
    // Set the layout of all headers in the table
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if tableView == self.listField
        {
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            header.textLabel?.textColor = UIColor.darkGray
        }
        else
        {
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.font = UIFont.systemFont(ofSize: 14)
            header.textLabel?.textColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        }
    }
    
    // When the cell is clicked, transfer the data to another view and navigate to that view. There are 6 sections so there are 6 segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectAnimalTypeSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! AnimalTypeSelectionViewController
            controller.selectedAnimal = self.selectedAnimal
        }
        if segue.identifier == "SelectAnimalDangerousSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! AnimalDangerousLevelViewController
            controller.selectedDangerousLevel = self.selectedDangerousLevel
        }
        if segue.identifier == "AntDetailSearchSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! SpeciesDetailTableViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = listField.indexPath(for: selectedCell)!
                let selectedItem = antSelection[indexPath.row].scienceName
                controller.speciesScientificName = selectedItem!
                let selectedItemB = antSelection[indexPath.row].commonName
                controller.speciesCommonName = selectedItemB!
                let selectedItemC = antSelection[indexPath.row].dangerousDes
                controller.speciesDangerousDes = selectedItemC!
                let selectedItemD = antSelection[indexPath.row].description
                controller.speciesDes = selectedItemD!
            }
        }
        if segue.identifier == "BeeDetailSearchSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! SpeciesDetailTableViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = listField.indexPath(for: selectedCell)!
                let selectedItem = beeSelection[indexPath.row].scienceName
                controller.speciesScientificName = selectedItem!
                let selectedItemB = beeSelection[indexPath.row].commonName
                controller.speciesCommonName = selectedItemB!
                let selectedItemC = beeSelection[indexPath.row].dangerousDes
                controller.speciesDangerousDes = selectedItemC!
                let selectedItemD = beeSelection[indexPath.row].description
                controller.speciesDes = selectedItemD!
            }
        }
        if segue.identifier == "MarsupialDetailSearchSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! SpeciesDetailTableViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = listField.indexPath(for: selectedCell)!
                let selectedItem = marsupialSelection[indexPath.row].scienceName
                controller.speciesScientificName = selectedItem!
                let selectedItemB = marsupialSelection[indexPath.row].commonName
                controller.speciesCommonName = selectedItemB!
                let selectedItemC = marsupialSelection[indexPath.row].dangerousDes
                controller.speciesDangerousDes = selectedItemC!
                let selectedItemD = marsupialSelection[indexPath.row].description
                controller.speciesDes = selectedItemD!
            }
        }
        if segue.identifier == "SnakeDetailSearchSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! SpeciesDetailTableViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = listField.indexPath(for: selectedCell)!
                let selectedItem = snakeSelection[indexPath.row].scienceName
                controller.speciesScientificName = selectedItem!
                let selectedItemB = snakeSelection[indexPath.row].commonName
                controller.speciesCommonName = selectedItemB!
                let selectedItemC = snakeSelection[indexPath.row].dangerousDes
                controller.speciesDangerousDes = selectedItemC!
                let selectedItemD = snakeSelection[indexPath.row].description
                controller.speciesDes = selectedItemD!
            }
        }
        if segue.identifier == "SpiderDetailSearchSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! SpeciesDetailTableViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = listField.indexPath(for: selectedCell)!
                let selectedItem = spiderSelection[indexPath.row].scienceName
                controller.speciesScientificName = selectedItem!
                let selectedItemB = spiderSelection[indexPath.row].commonName
                controller.speciesCommonName = selectedItemB!
                let selectedItemC = spiderSelection[indexPath.row].dangerousDes
                controller.speciesDangerousDes = selectedItemC!
                let selectedItemD = spiderSelection[indexPath.row].description
                controller.speciesDes = selectedItemD!
            }
        }
        if segue.identifier == "WaspDetailSearchSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! SpeciesDetailTableViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = listField.indexPath(for: selectedCell)!
                let selectedItem = waspSelection[indexPath.row].scienceName
                controller.speciesScientificName = selectedItem!
                let selectedItemB = waspSelection[indexPath.row].commonName
                controller.speciesCommonName = selectedItemB!
                let selectedItemC = waspSelection[indexPath.row].dangerousDes
                controller.speciesDangerousDes = selectedItemC!
                let selectedItemD = waspSelection[indexPath.row].description
                controller.speciesDes = selectedItemD!
            }
        }
    }

    // Set the alert function, so that it can be called when this function is called
    func showAlertWithDismiss(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertDismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertDismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Unwind segue, read from the setting page and store the setting information
    @IBAction func unwindToSearchAnimal(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? AnimalTypeSelectionViewController {
            self.selectedAnimal = sourceViewController.selectedAnimal
        }
        self.selectionField.reloadData()
        print(selectedAnimal)
    }
    
    // Unwind segue, read from the setting page and store the setting information
    @IBAction func unwindToSearchAnimalDanger(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? AnimalDangerousLevelViewController {
            self.selectedDangerousLevel = sourceViewController.selectedDangerousLevel
        }
        self.selectionField.reloadData()
        print(selectedAnimal)
    }
    
    // Actions done when the 'search' button is clicked
    @IBAction func search(_ sender: UIButton) {
        if self.selectedAnimal.isEmpty == true || self.selectedDangerousLevel.isEmpty == true
        {
            self.showAlertWithDismiss(title: "Selection Required", message: "You must select at least one animal and one dangerous level.")
            return
        }
        
        if self.antRetrieved.isEmpty == true, self.beeRetrieved.isEmpty == true, self.marsupialRetrieved.isEmpty == true, self.snakeRetrieved.isEmpty == true, self.spiderRetrieved.isEmpty == true, self.waspRetrieved.isEmpty == true
        {
            self.view.endEditing(true)
            self.activityWait.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            databaseRef.observeSingleEvent(of: .value, with: { (snapshot) in
                self.viewThisOccurenceData = snapshot.value as? NSArray
                var index = 0
                while index < (self.viewThisOccurenceData?.count)!
                {
                    let dictionary = self.viewThisOccurenceData![index] as! NSDictionary
                    if !self.categoryScienceName.contains(dictionary["sp_sci_name"] as! String)
                    {
                        self.categoryScienceName.append(dictionary["sp_sci_name"] as! String)
                        
                        let animal = Animal()
                        animal.animalName = dictionary["cls_cmn_name"] as! String
                        animal.scienceName = dictionary["sp_sci_name"] as! String
                        animal.commonName = dictionary["sp_cmn_name"] as! String
                        animal.dangerousLevel = dictionary["sp_dngr_lvl"] as! String
                        animal.dangerousDes = dictionary["sp_dngr_desc"] as! String
                        animal.description = dictionary["sp_desc"] as! String
                        
                        if dictionary["cls_cmn_name"] as! String == "Snakes"
                        {
                            self.snakeRetrieved.append(animal)
                            if self.selectedAnimal.contains("Snakes"), self.selectedDangerousLevel.contains(dictionary["sp_dngr_lvl"] as! String), (animal.commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                            {
                                self.snakeSelection.append(animal)
                            }
                        }
                        if dictionary["cls_cmn_name"] as! String == "Spiders"
                        {
                            self.spiderRetrieved.append(animal)
                            if self.selectedAnimal.contains("Spiders"), self.selectedDangerousLevel.contains(dictionary["sp_dngr_lvl"] as! String), (animal.commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                            {
                                self.spiderSelection.append(animal)
                            }
                        }
                        if dictionary["cls_cmn_name"] as! String == "Bees"
                        {
                            self.beeRetrieved.append(animal)
                            if self.selectedAnimal.contains("Bees"), self.selectedDangerousLevel.contains(dictionary["sp_dngr_lvl"] as! String), (animal.commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                            {
                                self.beeSelection.append(animal)
                            }
                        }
                        if dictionary["cls_cmn_name"] as! String == "Wasps"
                        {
                            self.waspRetrieved.append(animal)
                            if self.selectedAnimal.contains("Wasps"), self.selectedDangerousLevel.contains(dictionary["sp_dngr_lvl"] as! String), (animal.commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                            {
                                self.waspSelection.append(animal)
                            }
                        }
                        if dictionary["cls_cmn_name"] as! String == "Ants"
                        {
                            self.antRetrieved.append(animal)
                            if self.selectedAnimal.contains("Ants"), self.selectedDangerousLevel.contains(dictionary["sp_dngr_lvl"] as! String), (animal.commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                            {
                                self.antSelection.append(animal)
                            }
                        }
                        if dictionary["cls_cmn_name"] as! String == "Carnivorous Marsupials"
                        {
                            self.marsupialRetrieved.append(animal)
                            if self.selectedAnimal.contains("Carnivorous Marsupials"), self.selectedDangerousLevel.contains(dictionary["sp_dngr_lvl"] as! String), (animal.commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                            {
                                self.marsupialSelection.append(animal)
                            }
                        }
                    }
                    index = index + 1
                }
                
                // This is used to sort the all animal array according to the common name
                self.antSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
                self.beeSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
                self.marsupialSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
                self.snakeSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
                self.spiderSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
                self.waspSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
                
                self.listField.reloadData()
                self.listField.setContentOffset(CGPoint.zero, animated: true)
                self.activityWait.stopAnimating()
                self.view.isUserInteractionEnabled = true
                
                // If nothing found, then show an alert
                if self.antSelection.isEmpty == true, self.beeSelection.isEmpty == true, self.marsupialSelection.isEmpty == true, self.snakeSelection.isEmpty == true, self.spiderSelection.isEmpty == true, self.waspSelection.isEmpty == true
                {
                    self.showAlertWithDismiss(title: "No result found", message: "Please change your search (you can leave it blank or make it wider).")
                }
                // ...
            }) { (error) in
                print(error.localizedDescription)
            }
        }
        else
        {
            self.view.endEditing(true)
            self.activityWait.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            antSelection = []
            beeSelection = []
            marsupialSelection = []
            snakeSelection = []
            spiderSelection = []
            waspSelection = []
            
            if self.selectedAnimal.contains("Ants")
            {
                var index:Int = 0
                while index < antRetrieved.count
                {
                    if self.selectedDangerousLevel.contains(antRetrieved[index].dangerousLevel), (antRetrieved[index].commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                    {
                        self.antSelection.append(antRetrieved[index])
                    }
                    index = index + 1
                }
            }
            if self.selectedAnimal.contains("Bees")
            {
                var index:Int = 0
                while index < beeRetrieved.count
                {
                    if self.selectedDangerousLevel.contains(beeRetrieved[index].dangerousLevel), (beeRetrieved[index].commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                    {
                        self.beeSelection.append(beeRetrieved[index])
                    }
                    index = index + 1
                }
            }
            if self.selectedAnimal.contains("Carnivorous Marsupials")
            {
                var index:Int = 0
                while index < marsupialRetrieved.count
                {
                    if self.selectedDangerousLevel.contains(marsupialRetrieved[index].dangerousLevel), (marsupialRetrieved[index].commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                    {
                        self.marsupialSelection.append(marsupialRetrieved[index])
                    }
                    index = index + 1
                }
            }
            if self.selectedAnimal.contains("Snakes")
            {
                var index:Int = 0
                while index < snakeRetrieved.count
                {
                    if self.selectedDangerousLevel.contains(snakeRetrieved[index].dangerousLevel), (snakeRetrieved[index].commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                    {
                        self.snakeSelection.append(snakeRetrieved[index])
                    }
                    index = index + 1
                }
            }
            if self.selectedAnimal.contains("Spiders")
            {
                var index:Int = 0
                while index < spiderRetrieved.count
                {
                    if self.selectedDangerousLevel.contains(spiderRetrieved[index].dangerousLevel), (spiderRetrieved[index].commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                    {
                        self.spiderSelection.append(spiderRetrieved[index])
                    }
                    index = index + 1
                }
            }
            if self.selectedAnimal.contains("Wasps")
            {
                var index:Int = 0
                while index < waspRetrieved.count
                {
                    if self.selectedDangerousLevel.contains(waspRetrieved[index].dangerousLevel), (waspRetrieved[index].commonName.lowercased().range(of:self.searchField.text!.lowercased()) != nil || self.searchField.text == "")
                    {
                        self.waspSelection.append(waspRetrieved[index])
                    }
                    index = index + 1
                }
            }
            
            // This is used to sort the all animal array according to the common name
            self.antSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
            self.beeSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
            self.marsupialSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
            self.snakeSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
            self.spiderSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
            self.waspSelection.sort { $0.dangerousLevel > $1.dangerousLevel }
            
            if self.antSelection.isEmpty == true, self.beeSelection.isEmpty == true, self.marsupialSelection.isEmpty == true, self.snakeSelection.isEmpty == true, self.spiderSelection.isEmpty == true, self.waspSelection.isEmpty == true
            {
                self.showAlertWithDismiss(title: "No result found", message: "Please change your search (you can leave it blank or make it wider).")
                self.listField.reloadData()
                self.listField.setContentOffset(CGPoint.zero, animated: true)
                self.activityWait.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
            else
            {
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.listField.reloadData()
                    self.listField.setContentOffset(CGPoint.zero, animated: true)
                    self.activityWait.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                }
            }
        }
        self.checkState = true
    }
    
}
