//
//  AnimalDangerousLevelViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/4.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This view is used for users to select which dangerous level they want to display. All data will be returned to the originating view
class AnimalDangerousLevelViewController: UIViewController {

    @IBOutlet weak var allSwitch: UISwitch!
    @IBOutlet weak var highSwitch: UISwitch!
    @IBOutlet weak var mediumSwitch: UISwitch!
    @IBOutlet weak var lowSwitch: UISwitch!
    
    // Used to store the selected danger level to display
    var selectedDangerousLevel:[String] = []
    
    // Once the view is loaded, this function will be called
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the color of the navigation bar
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor
        
        // Set the state of the switch button when the view is loaded
        if selectedDangerousLevel.count == 0
        {
            highSwitch.setOn(false, animated: true)
            mediumSwitch.setOn(false, animated: true)
            lowSwitch.setOn(false, animated: true)
            allSwitch.setOn(false, animated: true)
        }
        else
        {
            var count:Int = 0
            if selectedDangerousLevel.contains("3")
            {
                highSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                highSwitch.setOn(false, animated: true)
            }
            if selectedDangerousLevel.contains("2")
            {
                mediumSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                mediumSwitch.setOn(false, animated: true)
            }
            if selectedDangerousLevel.contains("1")
            {
                lowSwitch.setOn(true, animated: true)
                count = count + 1
            }
            else
            {
                lowSwitch.setOn(false, animated: true)
            }
            if count == 3
            {
                allSwitch.setOn(true, animated: true)
            }
            else
            {
                allSwitch.setOn(false, animated: true)
            }
        }
    }
    
    // Set the alert function, so that it can be called when this function is called
    func showAlertWithDismiss(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertDismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertDismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Do some validations before performing the segue
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "unwindToSearchAnimalDangerSegue"
        {
            if highSwitch.isOn == false, mediumSwitch.isOn == false, lowSwitch.isOn == false
            {
                self.showAlertWithDismiss(title: "Selection Required", message: "You must select at least one dangerous level.")
                return false
            }
        }
        return true
    }
    
    // Set the function to be invoked when the save button is clicked
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToSearchAnimalDangerSegue"
        {
            selectedDangerousLevel = []
            if allSwitch.isOn == true
            {
                selectedDangerousLevel.append("3")
                selectedDangerousLevel.append("2")
                selectedDangerousLevel.append("1")
            }
            else
            {
                if highSwitch.isOn == true
                {
                    selectedDangerousLevel.append("3")
                }
                if mediumSwitch.isOn == true
                {
                    selectedDangerousLevel.append("2")
                }
                if lowSwitch.isOn == true
                {
                    selectedDangerousLevel.append("1")
                }
            }
        }
    }
    
    // Define what other switch button should do when the all switch is clicked
    @IBAction func allSwitchChanged(_ sender: UISwitch) {
        if allSwitch.isOn == true
        {
            highSwitch.setOn(true, animated: true)
            mediumSwitch.setOn(true, animated: true)
            lowSwitch.setOn(true, animated: true)
        }
        if allSwitch.isOn == false
        {
            highSwitch.setOn(false, animated: true)
            mediumSwitch.setOn(false, animated: true)
            lowSwitch.setOn(false, animated: true)
        }
    }
    
    // Define what other switch button should do when the high switch is clicked
    @IBAction func highSwitchChanged(_ sender: UISwitch) {
        if highSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if highSwitch.isOn == true, mediumSwitch.isOn == true, lowSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
    
    // Define what other switch button should do when the medium switch is clicked
    @IBAction func mediumSwitchChanged(_ sender: UISwitch) {
        if mediumSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if highSwitch.isOn == true, mediumSwitch.isOn == true, lowSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
    
    // Define what other switch button should do when the low switch is clicked
    @IBAction func lowSwitchChanged(_ sender: UISwitch) {
        if lowSwitch.isOn == false
        {
            allSwitch.setOn(false, animated: true)
        }
        if highSwitch.isOn == true, mediumSwitch.isOn == true, lowSwitch.isOn == true
        {
            allSwitch.setOn(true, animated: true)
        }
    }
    
    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Set the function to be invoked when the cancel button is clicked
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
