//
//  ListSpeciesSearchTableViewCell.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/5/6.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

class ListSpeciesSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var imageDisplayCell: UIImageView!
    @IBOutlet weak var commonNameCell: UILabel!
    @IBOutlet weak var scientificNameCell: UILabel!
    @IBOutlet weak var dangerousLevelCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
